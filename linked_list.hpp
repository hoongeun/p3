/*@file LinkedList.h */
#pragma once
#include <iostream>
#include <stdexcept>
#include <string>
#include <cstddef>
#include "abstract_list.hpp"

class PrecondViolatedExcep : public std::logic_error
{
public:
  PrecondViolatedExcep(const std::string& message = "");
}; // end PrecondViolatedExcep

template <class ItemType>
class Node {
private :
    ItemType item; // A data item
    Node<ItemType>* next; // Pointer to next node
public :
    Node();
    Node( const ItemType& anItem);
    Node( const ItemType& anItem, Node<ItemType>* nextNodePtr);
    void setItem( const ItemType& anItem);
    void setNext(Node<ItemType>* nextNodePtr);
    ItemType getItem() const ;
    Node<ItemType>* getNext() const ;
}; // end Node

template <class ItemType>
class LinkedList : public AbstractList<ItemType>
{
 private :
    Node<ItemType>* headPtr; // Pointer to first node in the chain
    // (contains the first entry in the list)
    int itemCount; // Current count of list items
    // Locates a specified node in a linked list.
    // @pre position is the number of the desired node;
    // position >= 1 and position <= itemCount.
    // @post The node is found and a pointer to it is returned.
    // @param position The number of the node to locate.
    // @return A pointer to the node at the given position.
    Node<ItemType>* getNodeAt(int position) const;
 public :
    LinkedList();
    LinkedList(const LinkedList<ItemType>& other);
    virtual ~LinkedList();
    bool isEmpty() const noexcept;
    std::size_t getLength() const noexcept;
    void insert(std::size_t newPosition, const ItemType& newEntry);
    void remove(std::size_t position);
    void clear() noexcept;
    ItemType getEntry(std::size_t position) const;
    Node<ItemType>* getNodeAt(std::size_t position) const;
    void setEntry(std::size_t position, const ItemType& newEntry);
}; // end

PrecondViolatedExcep::PrecondViolatedExcep(const std::string& message): std::logic_error("Precondition Violated Exception: " + message)
{
}  // end constructor

/////////////////////////////////////////////////////////////////////////
// Node

template <class ItemType>
Node<ItemType>::Node() : next( nullptr )
{
} // end default constructor
template <class ItemType>
Node<ItemType>::Node( const ItemType& anItem) : item(anItem), next( nullptr )
{
} // end constructor
template <class ItemType>
Node<ItemType>::Node( const ItemType& anItem, Node<ItemType>* nextNodePtr) :
item(anItem), next(nextNodePtr)
{

} // end constructor
template <class ItemType>
void Node<ItemType>::setItem( const ItemType& anItem)
{
    item = anItem;
} // end setItem
template <class ItemType>
void Node<ItemType>::setNext(Node<ItemType>* nextNodePtr)
{
    next = nextNodePtr;
} // end setNext
template <class ItemType>
ItemType Node<ItemType>::getItem() const
{
    return item;
} // end getItem
template <class ItemType>
Node<ItemType>* Node<ItemType>::getNext() const
{
    return next;
}

/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// LinkedList

template <class ItemType>
LinkedList<ItemType>::LinkedList() : headPtr(nullptr), itemCount(0)
{
} // end default constructor

template <class ItemType>
LinkedList<ItemType>::LinkedList(const LinkedList<ItemType>& other)
{
    this->headPtr = nullptr;
    if (other.itemCount > 0)
    {
        for (std::size_t pos = 0; pos < other.getLength(); pos++)
        {
            ItemType item  = other.getEntry(pos + 1);
            this->insert(pos + 1, item);
        }
    }
} // end default constructor

template <class ItemType>
LinkedList<ItemType>::~LinkedList()
{
    clear();
} // end destructor

template <class ItemType>
bool LinkedList<ItemType>::isEmpty() const noexcept
{
    return getLength() == 0;
} // end getEntry

template <class ItemType>
std::size_t LinkedList<ItemType>::getLength() const noexcept
{
    return itemCount;
} // end getEntry

template <class ItemType>
ItemType LinkedList<ItemType>::getEntry(std::size_t position) const
{
    // Enforce precondition
    bool ableToGet = (position >= 1) && (position <= itemCount);
    if (!ableToGet)
    {
        std::string message = "getEntry() called with an empty list or ";
        message = message + "invalid position.";
        throw(PrecondViolatedExcep(message));
    }

    Node<ItemType>* nodePtr = getNodeAt(position);
    return nodePtr->getItem();
} // end getEntry

template <class ItemType>
Node<ItemType>* LinkedList<ItemType>::getNodeAt(std::size_t position) const
{
    Node<ItemType>* ptr = headPtr;

	if (ptr != nullptr && itemCount > 0)
	{
		for (std::size_t pos = 1; pos < position; pos++)
		{
			ptr = ptr->getNext();
		}
	}
	return ptr;
}

template <class ItemType>
void LinkedList<ItemType>::setEntry(std::size_t position, const ItemType& item)
{
	// Enforce precondition
	bool ableToSet = (position >= 1) && (position <= itemCount);
	if (!ableToSet)
	{
		std::string message = "setEntry() called with an empty list or ";
		message = message + "invalid position.";
		throw(PrecondViolatedExcep(message));
	}

	Node<ItemType>* nodePtr = getNodeAt(position);
	nodePtr->setItem(item);
} // end getEntry

template <class ItemType>
void LinkedList<ItemType>::insert(std::size_t newPosition,
                                  const ItemType& newEntry)
{
    bool ableToInsert = (newPosition >= 1) &&
        (newPosition <= itemCount + 1);
    if (!ableToInsert)
    {
        std::string message = "getEntry() called with an empty list or ";
        message = message + "invalid position.";
        throw(PrecondViolatedExcep(message));
        return;
    }

    // Create a new node containing the new entry
    Node<ItemType>* newNodePtr = new Node<ItemType>(newEntry);
    // Attach new node to chain
    if (newPosition == 1)
    {
        // Insert new node at beginning of chain
        newNodePtr->setNext(headPtr);
        headPtr = newNodePtr;
    }
    else
    {
        // Find node that will be before new node
        Node<ItemType>* prevPtr = getNodeAt(newPosition - 1);
        // Insert new node after node to which prevPtr points
        newNodePtr->setNext(prevPtr->getNext());
        prevPtr->setNext(newNodePtr);
    } // end if
    itemCount++; // Increase count of entries
} // end insert

template <class ItemType>
void LinkedList<ItemType>::remove(std::size_t position)
{
    bool ableToRemove = (position >= 1) && (position <= itemCount);
    if (!ableToRemove)
    {
        std::string message = "getEntry() called with an empty list or ";
        message = message + "invalid position.";
        throw(PrecondViolatedExcep(message));
        return;
    }

    Node<ItemType>* curPtr = nullptr ;
    if (position == 1)
    {
        // Remove the first node in the chain
        curPtr = headPtr; // Save pointer to node
        headPtr = headPtr->getNext();
    }
    else
    {
        // Find node that is before the one to delete
        Node<ItemType>* prevPtr = getNodeAt(position - 1);
        // Point to node to delete
        curPtr = prevPtr->getNext();
        // Disconnect indicated node from chain by connecting the
        // prior node with the one after
        prevPtr->setNext(curPtr->getNext());
    } // end if
    // Return node to system
    curPtr->setNext( nullptr );
    delete curPtr;
    curPtr = nullptr ;
    itemCount--; // Decrease count of entries
} // end remove

template <class ItemType>
void LinkedList<ItemType>::clear() noexcept
{
    while (!isEmpty())
        remove(1);
} // end clear

#if 0
// The public method insert:
template <class ItemType>
void LinkedList<ItemType>::insert(std::size_t newPosition, const ItemType& newEntry)
{
    bool ableToInsert = (newPosition >= 1) &&
        (newPosition <= itemCount + 1);
    if (!ableToInsert)
    {
        std::string message = "getEntry() called with an empty list or ";
        message = message + "invalid position.";
        throw(PrecondViolatedExcep(message));
        return;
    }
    // Create a new node containing the new entry
    Node<ItemType>* newNodePtr = new Node<ItemType>(newEntry);
    headPtr = insertNode(newPosition, newNodePtr, headPtr);
} // end insert

template <class ItemType>
Node<ItemType>* LinkedList<ItemType>::insertNode(std::size_t position,
                                   Node<ItemType>* newNodePtr,
                                   Node<ItemType>* subChainPtr)
{
    if (position == 1)
    {
        // Insert new node at beginning of subchain
        newNodePtr->setNext(subChainPtr);
        subChainPtr = newNodePtr;
        itemCount++; // Increase count of entries
    }
    else
    {
        Node<ItemType>* afterPtr = insertNode(position - 1, newNodePtr, subChainPtr->getNext());
        subChainPtr->setNext(afterPtr);
    } // end if
    return subChainPtr;
} // end insertNode
#endif

/////////////////////////////////////////////////////////////////////////
