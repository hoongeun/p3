// implement your main rentry point for the todo applicaiton here
#include <iostream>
#include <cstdlib>
#include <string>
#include "linked_list.hpp"

int main(int argc, char *argv[])
{
    LinkedList<int>* pList = new LinkedList<int>();
    pList->insert(1, 5);
	pList->insert(pList->getLength() + 1, 1);
	pList->insert(pList->getLength() + 1, 3);
	pList->remove(1);
    int a = pList->getEntry(1);
	std::cout << "expect 1:" << a << std::endl;
	pList->setEntry(2, 7);
	a = pList->getEntry(2);
	std::cout << "expect 7:" << a << std::endl;
	std::cout << "expect 2:" << pList->getLength() << std::endl;
	pList->clear();
	bool b = pList->isEmpty();
	std::cout << "expect true: " << b << std::endl;

    return EXIT_SUCCESS;
}
