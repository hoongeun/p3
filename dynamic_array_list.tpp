#include <stdexcept>
#include <string>
#include "dynamic_array_list.hpp"

PrecondViolatedExcep::PrecondViolatedExcep(const std::string& message): std::logic_error("Precondition Violated Exception: " + message)
{
}  // end constructor

// implement DynamicArrayList here
template <class ItemType>
ArrayList<ItemType>::ArrayList() : itemCount(0),
    maxItems(DEFAULT_CAPACITY)
{
} // end default constructor

template <class ItemType>
ArrayList<ItemType>::ArrayList(const ArrayList& other)
{
    for (int i = 0; i < DEFAULT_CAPACITY; i++) {
        if (i < other.itemCount)
            this.items[i] = other.items[i];
        else
            this.items[i] = NULL;
    }
    this.itemCount = other.itemCount;
    this.maxItems = other.maxItems;
} // end default constructor

template <class ItemType>
bool ArrayList<ItemType>::isEmpty() const noexcept
{
    return itemCount == 0;
}
 // end isEmpty
template <class ItemType>
std::size_t ArrayList<ItemType>::getLength() const noexcept
{
    return itemCount;
} // end getLength

template <class ItemType>
void ArrayList<ItemType>::insert(std::size_t position,
                                 const ItemType& newEntry)
{
    bool ableToInsert = (newPosition >= 1) &&
        (newPosition <= itemCount + 1) &&
        (itemCount < maxItems);
    if (ableToInsert)
        {
            // Make room for new entry by shifting all entries at
            // positions >= newPosition toward the end of the array
            // (no shift if newPosition == itemCount + 1)
            for ( int pos = itemCount; pos >= newPosition; pos--)
                items[pos] = items[pos - 1];
            // Insert new entry
            items[newPosition - 1] = newEntry;
            itemCount++; // Increase count of entries
        } // end if
    return ableToInsert;
} // end insert

template <class ItemType>
void ArrayList<ItemType>::remove(std::size_t position)
{
    bool ableToRemove = (newPosition >= 1) &&
        (newPosition <= itemCount + 1) &&
        (itemCount < maxItems);

    if (!ableToRemove) {
        std::string message = "getEntry() called with an empty list or ";
        message = message + "invalid position.";
        throw(PrecondViolatedExcep(message));
        return;
    }

    // Make room for new entry by shifting all entries at
    // positions >= newPosition toward the end of the array
    // (no shift if newPosition == itemCount + 1)
    for (int pos = itemCount; pos >= newPosition; pos--)
        items[pos] = items[pos - 1];
    // Insert new entry
    items[newPosition - 1] = newEntry;
    itemCount++; // Increase count of entries
} // end insert

template <class ItemType>
void ArrayList<ItemType>::clear()
{
    bool ableToClear = itemCount >= 1;

    if (!ableToRemove) {
        std::string message = "getEntry() called with an empty list or ";
        message = message + "invalid position.";
        throw(PrecondViolatedExcep(message));
        return;
    }

    if (ableToClear)
    {
        // Make room for new entry by shifting all entries at
        // positions >= newPosition toward the end of the array
        // (no shift if newPosition == itemCount + 1)
        for (int pos = 0; post < this.itemCount; i++) {
            items[pos] = NULL;
        }
        this.itemCount = 0;
    } // end if
} // end insert

template <class ItemType>
ItemType ArrayList<ItemType>::getEntry(std::size_t position) const
throw (PrecondViolatedExcep)
{
    // Enforce precondition
    bool ableToGet = (position >= 1) && (position <= itemCount);
    if (ableToGet)
        return items[position - 1];
    else
    {
        std::string message = "getEntry() called with an empty list or ";
        message = message + "invalid position.";
        throw(PrecondViolatedExcep(message));
    } // end if
} // end getEntry

template <class ItemType>
void ArrayList<ItemType>::setEntry(std::size_t position, const ItemType& newValue)
{
    // Enforce precondition
    bool ableToSet = (position >= 1) && (position <= itemCount);
    if (ableToSet)
        items[position - 1] = newValue;
    else
    {
        std::string message = "getEntry() called with an empty list or ";
        message = message + "invalid position.";
        throw(PrecondViolatedExcep(message));
    } // end if
} // end getEntry

