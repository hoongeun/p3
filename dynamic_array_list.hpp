#pragma once
#include "abstract_list.hpp"

template < class ItemType>
class ArrayList : public AbstractList<ItemType>
{
private :
    static const int DEFAULT_CAPACITY = 100;
    ItemType items[DEFAULT_CAPACITY]; // Array of list items
    int itemCount; // Current count of list items
    int maxItems; // Maximum capacity of the list
public :
    ArrayList();
    ArrayList(const ArrayList& other);
    ~ArrayList();
    // Copy constructor and destructor are supplied by compiler

    // determine if a list is empty
    bool isEmpty() const noexcept;

    // return current length of the list
    std::size_t getLength() const noexcept;

    // insert item at position in the list using 0-based indexing
    // throws std::range_error if position is invalid
    void insert(std::size_t position, const ItemType& item);

    // remove item at position in the list using 0-based indexing
    // throws std::range_error if position is invalid
    void remove(std::size_t position);

    // remove all items from the list
    void clear() noexcept ;

    // get a copy of the item at position using 0-based indexing
    // throws std::range_error if position is invalid
    ItemType getEntry(std::size_t position) const;

    // set the value of the item at position using 0-based indexing
    // throws std::range_error if position is invalid
    void setEntry(std::size_t position, const ItemType& newValue);
}; // end ArrayList
